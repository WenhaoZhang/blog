# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = 'bc9f997c593f64974d611a36bf51b6eae24bb5303c5593d600a70344922c66a8b30343be58a4a1ed387d25903247112cd495d7481155dc6d84a081736bbd1e30'
